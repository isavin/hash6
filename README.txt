The hypothesis: "Three unsynchronized concurrent HashMap.put() mutations
using keys which have hashcodes resolving to the same bucket (the hashcodes
can be different) can create loops in the single-linked list used to store
the bucket entries. As a result operations which iterate over the bucket
linked-list and don't break out of the iteration before hitting the loop will
remain stuck forever. This breaks the liveness contract. Concrete exemple of
such an iteration: get() using a key which is assigned to that specific bucket
and is not present in the map."

I ran into this issue in production about 10 years ago and it looks like it
was reported elsewhere also:
https://bugs.java.com/bugdatabase/view_bug.do?bug_id=7027300

Back then I didn't understand the exact steps which triggered the loop creation
but I was able to identify them on this occasion.

The byteman script is based on the source code of HashMap from version 6u45
but the issue is probably present in all jdk6 versions. (possibly other major
versions as well, didn't have time to check). For some byteman rules I'm using
line numbers to inject the code and they might not match other updates/versions.

Byteman (https://downloads.jboss.org/byteman/4.0.9/byteman-programmers-guide.html)
is a tool capable of injecting bytecode at runtime at specific location
and provides primitives which can be used to orchestrate the order in which
threads execute code. It has other capabilities but this is what I use
it for in this case.

What would happen if only one thread would mutate the map using
HashMap.put():
 - inside HashMap.put() the hashcode of the key is used to identify the
bucket in which the entry should be located
 - each bucket is represented as a singly linked list of Map.Entry objects
with the head of the list stored in an array (the HashMap.table field)
 - after the bucket is identified, the code goes through the linked list and
looks for entry with a matching key (hashcode and equals)
 - if none if found a new entry will be added using a call to
HashMap.addEntry()
 - HashMap.addEntry() retrieves the head of the linked list, creates a new
HashMap.Entry which has .next pointing to the old list head (or null if empty) and it
them replaces the head of the list with this new entry
 - since the capacity of the list was given as 1 when the map was created,
the new size of 1 will be over the occupation threshold 1*0.75 and it will trigger a resize
 - HashMap.resize() creates a new Entry array to replace the old one (HashMap.table) and
passes it to the method HashMap.transfer() which is reponsible with moving
data (heads of the linked lists for each bucket) from the old table to the
new one
 - HashMap.transfer() retrieves each linked lists and goes through it
copying entries one by one to the new one
 - important to note: the new list will contain the same Entry objects (not
copies) and they will be in reverse order (e.g. 3->2->1->null becomes
1->2->3->null)
 - after the transfer is complete, HashMap.table is replaced by the newly
created one

This is one way of triggering the loop creation. There could be more.

The key to obtaining the loop is to get two threads to perform a resize on
the linked-list of a bucket but having different views of that list. The
first thread sees the elements in normal order (2->1->null) while the second
thread sees the list in reversed order (1->2->null).

A third thread is used to obtain this asymmetry. It reverse the list after
the first thread read observed it in the "normal order". The element the
third thread is supposed to add is dropped (exposing another contract breach)
to keep the list simple.

The first thread
* sees the old list as 2->1->null
* works on Entry "2" and sets it as the head of the list, updates 2.next=null (new list: 2->null)
* works on Entry "1" and sets 1.next=2 (which is the head of the new list) <--- this is the point where half of the loop is created
* works on null and terminates

The second thread
* sees the old list as 1->2->null
* works on Entry "1" and sets it as the head of the list, updates 1.next=null (new list: 1->null)
* works on Entry "2" and sets 2.next=1 (which is the head of the new list) <--- this is the point where the other half of the loop is created
* works on null and terminates

The rules in the byteman script are listed time-ordered and the rule name
contains a brief description of what each step does. Please let me know if
it's not clear and need more details. They are quiet specific to the underlying
code but all they do is maintain the illusion that the bucket linked-list is
ordered differently for each thread.

Using maven 3.2.5 (https://archive.apache.org/dist/maven/maven-3/3.2.5/binaries/)
and jdk6u45 (https://www.oracle.com/java/technologies/javase-java-archive-javase6-downloads.html),
running this should be sufficient to reproduce the issue:

$ mvn clean install

The mvn output should stop at a line similar to this one:
"The test should now block (10 minutes test timeout). Use 'jstack 24631' to find the 'Time-limited test' thread stuck in HashMap.get() at line 303."

Test: src/test/java/hash6/ThreadSafetyTest.java
Byteman script: src/test/resources/hash6.ThreadSafetyTest.testInfiniteLoop.btm
