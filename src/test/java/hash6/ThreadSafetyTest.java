package hash6;

import java.lang.management.ManagementFactory;
import java.util.HashMap;

import org.jboss.byteman.contrib.bmunit.BMScript;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(org.jboss.byteman.contrib.bmunit.BMUnitRunner.class)
public class ThreadSafetyTest {
	private static class MapMutator implements Runnable {
		private final HashMap<Object, String> map;
		private final Object key;
		private final String value;

		public MapMutator(HashMap<Object, String> map, Object key, String value) {
			this.map = map;
			this.key = key;
			this.value = value;
		}

		@Override
		public void run() {
			map.put(key, value);
		}
	}

	@Test(timeout = 600000)
//	@BMUnitConfig(debug = true, verbose = true, bmunitVerbose = true)
	@BMScript(value = "hash6.ThreadSafetyTest.testInfiniteLoop.btm")
	public void testInfiniteLoop() throws InterruptedException {
		// Make the HashMap resize after one element is added.
		final HashMap<Object, String> map = new HashMap<Object, String>(1);

		// 5 keys, the first 4 have the same hashcode, the 5th has a different one.
		Object key1 = new Object() {
			@Override
			public int hashCode() {
				return 42;
			}
		};
		Object key2 = new Object() {
			@Override
			public int hashCode() {
				return 42;
			}
		};
		Object key3 = new Object() {
			@Override
			public int hashCode() {
				return 42;
			}
		};
		Object key4 = new Object() {
			@Override
			public int hashCode() {
				return 42;
			}
		};
		Object key5 = new Object() {
			@Override
			public int hashCode() {
				return 4242;
			}
		};

		// Start 3 threads which mutate the map concurrently. The exact order in which
		// the threads get to execute
		// the code is manipulated by the Byteman script specified in the @BMScript
		// method annotation.
		//
		// The MapMutator class exists just to make it easier to identify the HashMap
		// calls coming from the test vs
		// other interactions coming from Byteman or the JVM.
		Thread thread1 = new Thread(new MapMutator(map, key1, new String("a value")), "TestThread1");
		Thread thread2 = new Thread(new MapMutator(map, key2, new String("another value")), "TestThread2");
		Thread thread3 = new Thread(new MapMutator(map, key3, new String("yet another value")), "TestThread3");

		// Starts the mutators.
		thread1.start();
		thread2.start();
		thread3.start();

		// Blocks until all the mutators terminated.
		thread1.join();
		thread2.join();
		thread3.join();

		// The lines below display behavior consistent with the HashMap contract.
		// The following 3 calls respects the contract as the hashcode is different and
		// all the operations
		// are done on the linked list of another bucket.
		Assert.assertNull("Never added to the map.", map.get(key5));
		map.put(key5, "a fifth value in a different bucket");
		Assert.assertEquals("a fifth value in a different bucket", map.get(key5));

		// The following 2 calls respect the contract as the get() will find a matching
		// key value in the linked list
		// and will break out of the loop between the Entry corresponding to key 1 and
		// the one corresponding to key2.
		Assert.assertEquals("a value", map.get(key1));
		Assert.assertEquals("another value", map.get(key2));

		System.out.printf("The test should now block (10 minutes test timeout). Use 'jstack %s' to find the 'Time-limited test' thread stuck in HashMap.get() at line 303.%n", ManagementFactory.getRuntimeMXBean().getName().replaceAll("@.*", ""));
		// This line displays behavior inconsistent with the HashMap contract:
		// (1) key3 is no longer present in the map though the put() call for it
		// succeeded and no remove() was performed
		// (2) this get() will never return breaking the liveness contract (this
		// behavior will be the same for any key
		// with hashcode 42 not present int the hash map;
		Assert.assertNotNull("If you executed this test with JDK 6u45 the get() method will never return.",
				map.get(key3));
	}
}
